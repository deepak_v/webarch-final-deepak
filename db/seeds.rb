# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Priority.create(name: 'High')
Priority.create(name: 'Standard')
Priority.create(name: 'Standard or Low')
Priority.create(name: 'Project')

RequestType.create(name: 'Internal regular reporting and analsis of official data')
RequestType.create(name: 'Internal ad-hoc reporting and analsis of official data')
RequestType.create(name: 'External regular reporting of official data')
RequestType.create(name: 'External ad-hoc reporting of official data')
RequestType.create(name: 'Other')

Campu.create(name: 'Main Campus')
Campu.create(name: 'Gallup')
Campu.create(name: 'Los Almos')
Campu.create(name: 'Taos')
Campu.create(name: 'Valencia')

Student.create(name: 'All Students')
Student.create(name: 'Undergraduate')
Student.create(name: 'Freshmen')
Student.create(name: 'Full-Time')

YouAre.create(name: 'Faculty')
YouAre.create(name: 'Student')
YouAre.create(name: 'Staff')
YouAre.create(name: 'Other')
