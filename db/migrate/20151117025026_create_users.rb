class CreateUsers < ActiveRecord::Migration
  def change
    create_table :users do |t|
      t.string :first_namme
      t.string :last_name
      t.integer :phone_number
      t.string :email_id
      t.string :unit
      t.string :role

      t.timestamps null: false
    end
  end
end
