class CreateTickets < ActiveRecord::Migration
  def change
    create_table :tickets do |t|
      t.string :first_namme
      t.string :last_name
      t.string :job_title
      t.integer :phone_number
      t.string :department
      t.date :needed_by
      t.string :email_id
      t.string :priority
      t.string :are_you
      t.string :type_of_request
      t.text :purpose_of_request
      t.text :description_of_request
      t.integer :time_period
      t.string :student_type
      t.string :campus
      t.integer :irb
      t.string :grant_pi
      t.references :user, index: true, foreign_key: true

      t.timestamps null: false
    end
  end
end
