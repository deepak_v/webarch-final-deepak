json.array!(@tickets) do |ticket|
  json.extract! ticket, :id, :first_namme, :last_name, :job_title, :phone_number, :department, :needed_by, :email_id, :priority, :are_you, :type_of_request, :purpose_of_request, :description_of_request, :time_period, :student_type, :campus, :irb, :grant_pi, :user_id
  json.url ticket_url(ticket, format: :json)
end
