require 'test_helper'

class TicketsControllerTest < ActionController::TestCase
  setup do
    @ticket = tickets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:tickets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create ticket" do
    assert_difference('Ticket.count') do
      post :create, ticket: { are_you: @ticket.are_you, campus: @ticket.campus, department: @ticket.department, description_of_request: @ticket.description_of_request, email_id: @ticket.email_id, first_namme: @ticket.first_namme, grant_pi: @ticket.grant_pi, irb: @ticket.irb, job_title: @ticket.job_title, last_name: @ticket.last_name, needed_by: @ticket.needed_by, phone_number: @ticket.phone_number, priority: @ticket.priority, purpose_of_request: @ticket.purpose_of_request, student_type: @ticket.student_type, time_period: @ticket.time_period, type_of_request: @ticket.type_of_request, user_id: @ticket.user_id }
    end

    assert_redirected_to ticket_path(assigns(:ticket))
  end

  test "should show ticket" do
    get :show, id: @ticket
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @ticket
    assert_response :success
  end

  test "should update ticket" do
    patch :update, id: @ticket, ticket: { are_you: @ticket.are_you, campus: @ticket.campus, department: @ticket.department, description_of_request: @ticket.description_of_request, email_id: @ticket.email_id, first_namme: @ticket.first_namme, grant_pi: @ticket.grant_pi, irb: @ticket.irb, job_title: @ticket.job_title, last_name: @ticket.last_name, needed_by: @ticket.needed_by, phone_number: @ticket.phone_number, priority: @ticket.priority, purpose_of_request: @ticket.purpose_of_request, student_type: @ticket.student_type, time_period: @ticket.time_period, type_of_request: @ticket.type_of_request, user_id: @ticket.user_id }
    assert_redirected_to ticket_path(assigns(:ticket))
  end

  test "should destroy ticket" do
    assert_difference('Ticket.count', -1) do
      delete :destroy, id: @ticket
    end

    assert_redirected_to tickets_path
  end
end
